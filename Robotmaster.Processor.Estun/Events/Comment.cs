﻿// <copyright file="Comment.cs" company="Hypertherm Inc.">
// Copyright (c) Hypertherm Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Estun.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Comment event.
    /// </summary>
    [DataContract(Name = "E6A55FE0-E487-4E6E-9490-AE231B2B4BCD")]
    public class Comment : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="Comment"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public Comment()
        {
            this.ValidEventIndexes = EventIndex.Before | EventIndex.After;
        }

        /// <summary>
        ///     Gets or sets the comment text.
        /// </summary>
        [DataMember(Name = "0F010B0B-8BEA-4B09-8163-FBC67349A4D0")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public string CommentText { get; set; } = string.Empty;

        /// <summary>
        ///     Overrides of the method used to displayed the event in the point list.
        /// </summary>
        /// <returns>
        ///     The information displayed in the point list.
        /// </returns>
        public override string ToString()
        {
            return $"! {this.CommentText}";
        }

        /// <summary>
        ///     Overrides of the method used to write the event in the robot code.
        /// </summary>
        /// <returns>
        ///     The <c>string</c> to be written in the robot code.
        /// </returns>
        public override string ToCode()
        {
            return this.ToString();
        }
    }
}
