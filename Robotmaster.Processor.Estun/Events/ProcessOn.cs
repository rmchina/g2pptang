﻿// <copyright file="ProcessOn.cs" company="Hypertherm Inc.">
// Copyright (c) Hypertherm Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Estun.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Process on event.
    /// </summary>
    [DataContract(Name = "190F7266-1520-4239-BE0E-272126B5353C")]
    public class ProcessOn : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ProcessOn"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public ProcessOn()
        {
            this.ValidEventIndexes = EventIndex.Before | EventIndex.After;
        }

        /// <summary>
        ///     Gets or sets the process activation command.
        /// </summary>
        [DataMember(Name = "4D56D279-B683-48C4-B6EE-2F4B0CEC6A2B")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public string ProcessActivationCommand { get; set; } = "TOOLON";

        /// <summary>
        ///     Overrides of the method used to displayed the event in the point list.
        /// </summary>
        /// <returns>
        ///     The information displayed in the point list.
        /// </returns>
        public override string ToString()
        {
            return $"CALL {this.ProcessActivationCommand}";
        }

        /// <summary>
        ///     Overrides of the method used to write the event in the robot code.
        /// </summary>
        /// <returns>
        ///     The <c>string</c> to be written in the robot code.
        /// </returns>
        public override string ToCode()
        {
            return this.ToString();
        }
    }
}
