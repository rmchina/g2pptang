﻿// <copyright file="Command.cs" company="Hypertherm Inc.">
// Copyright (c) Hypertherm Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Estun.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Generic command event.
    /// </summary>
    [DataContract(Name = "F1B53262-8683-4608-A4D8-6AEDC07AC6E4")]
    public class Command : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="Command"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public Command()
        {
        }

        /// <summary>
        ///     Gets or sets the command text.
        /// </summary>
        [DataMember(Name = "974C0714-5130-4CC2-BDF9-3DBDF127B7F9")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public string CommandText { get; set; } = string.Empty;

        /// <summary>
        ///     Overrides of the method used to displayed the event in the point list.
        /// </summary>
        /// <returns>
        ///     The <c>string</c> displayed in the point list.
        /// </returns>
        public override string ToString()
        {
            return $"{this.CommandText}";
        }

        /// <summary>
        ///     Overrides of the method used to write the event in the robot code.
        /// </summary>
        /// <returns>
        ///     The <c>string</c> to be written in the robot code.
        /// </returns>
        public override string ToCode()
        {
            return this.ToString();
        }
    }
}