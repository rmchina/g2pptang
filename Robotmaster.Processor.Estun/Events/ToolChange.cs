﻿// <copyright file="ToolChange.cs" company="Hypertherm Inc.">
// Copyright (c) Hypertherm Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Estun.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Generic command event.
    /// </summary>
    [DataContract(Name = "8D5E1BA2-F094-4827-9E68-490CFF91CFE2")]
    public class ToolChange : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ToolChange"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public ToolChange()
        {
            this.ValidEventIndexes = EventIndex.Before | EventIndex.After;
        }

        /// <summary>
        ///     Gets or sets the command text.
        /// </summary>
        [DataMember(Name = "56B8E933-ADBB-4BB9-B17F-B32202619172")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public string ToolChangeCommand { get; set; } = string.Empty;

        /// <summary>
        ///     Overrides of the method used to displayed the event in the point list.
        /// </summary>
        /// <returns>
        ///     The <c>string</c> displayed in the point list.
        /// </returns>
        public override string ToString()
        {
            return $"{this.ToolChangeCommand}";
        }

        /// <summary>
        ///     Overrides of the method used to write the event in the robot code.
        /// </summary>
        /// <returns>
        ///     The <c>string</c> to be written in the robot code.
        /// </returns>
        public override string ToCode()
        {
            return this.ToString();
        }
    }
}