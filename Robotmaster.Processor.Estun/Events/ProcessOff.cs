﻿// <copyright file="ProcessOff.cs" company="Hypertherm Inc.">
// Copyright (c) Hypertherm Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Estun.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Process Off event.
    /// </summary>
    [DataContract(Name = "E3545902-8703-41E6-9E29-A9C6086BC747")]
    public class ProcessOff : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ProcessOff"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public ProcessOff()
        {
            this.ValidEventIndexes = EventIndex.Before | EventIndex.After;
        }

        /// <summary>
        ///     Gets or sets the process deactivation command.
        /// </summary>
        [DataMember(Name = "50B86491-66E2-4A58-8E69-ABAAA9B3CFF4")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public string ProcessDeactivationCommand { get; set; } = "TOOLOFF";

        /// <summary>
        ///     Overrides of the method used to displayed the event in the point list.
        /// </summary>
        /// <returns>
        ///     The information displayed in the point list.
        /// </returns>
        public override string ToString()
        {
            return $"CALL {this.ProcessDeactivationCommand}";
        }

        /// <summary>
        ///     Overrides of the method used to write the event in the robot code.
        /// </summary>
        /// <returns>
        ///     The <c>string</c> to be written in the robot code.
        /// </returns>
        public override string ToCode()
        {
            return this.ToString();
        }
    }
}
