//-----------------------------------------------------------------------------------
// <auto-generated>
//     This file was generated by the Robotmaster Processor Definition Editor (PDE).
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//-----------------------------------------------------------------------------------

namespace Robotmaster.Processor.Estun.Generated.Classes
{
    /// <summary>
    ///     This is a concrete implementation of the generated home operation interface.
    /// </summary>
    [global::Robotmaster.Processor.Core.Common.Attributes.DslVersionAttribute(3)]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Robotmaster.ProcessorDefinitionEditor.GenerationServices", "7.5.12424.0")]
    internal sealed class HomeOperation : global::Robotmaster.Processor.Core.Common.BaseClasses.HomeOperationBase, global::Robotmaster.Processor.Estun.Generated.Interfaces.IHomeOperation
    {
        private global::System.Collections.Generic.IReadOnlyList<global::Robotmaster.Processor.Estun.Generated.Interfaces.IPoint> points = null;

        private global::Robotmaster.Processor.Estun.Generated.Artifacts.Menus.Interfaces.IOperationMenus operationMenus = null;

        private HomeOperation(global::Robotmaster.Processor.Estun.Generated.Interfaces.IProgram program, global::Robotmaster.ComponentBasedTree.CbtNode operationNode, global::System.Int32 operationListIndex)
            : base(program, operationNode, operationListIndex)
        {
        }

        /// <summary>
        ///     Gets, if possible, the previous operation from the same program as this operation; if this is the first operation in the program, <see langword="null"/> is returned instead.
        /// </summary>
        public new global::Robotmaster.Processor.Estun.Generated.Interfaces.IOperation PreviousOperation => (global::Robotmaster.Processor.Estun.Generated.Interfaces.IOperation)base.PreviousOperation;

        /// <summary>
        ///     Gets, if possible, the next operation from the same program as this operation; if this is the last operation in the program, <see langword="null"/> is returned instead.
        /// </summary>
        public new global::Robotmaster.Processor.Estun.Generated.Interfaces.IOperation NextOperation => (global::Robotmaster.Processor.Estun.Generated.Interfaces.IOperation)base.NextOperation;

        /// <summary>
        ///     Gets, if possible, the previous task operation from the same program as this operation; if there is no previous task operation in the program, <see langword="null"/> is returned instead.
        /// </summary>
        public new global::Robotmaster.Processor.Estun.Generated.Interfaces.ITaskOperation PreviousTaskOperation => (global::Robotmaster.Processor.Estun.Generated.Interfaces.ITaskOperation)base.PreviousTaskOperation;

        /// <summary>
        ///     Gets, if possible, the next task operation from the same program as this operation; if there is no next task operation in the program, <see langword="null"/> is returned instead.
        /// </summary>
        public new global::Robotmaster.Processor.Estun.Generated.Interfaces.ITaskOperation NextTaskOperation => (global::Robotmaster.Processor.Estun.Generated.Interfaces.ITaskOperation)base.NextTaskOperation;

        /// <summary>
        ///    Gets the set of points of interest for this operation.
        /// </summary>
        public new global::Robotmaster.Processor.Estun.Generated.Interfaces.IOperationPointsOfInterest PointsOfInterest => (global::Robotmaster.Processor.Estun.Generated.Interfaces.IOperationPointsOfInterest)base.PointsOfInterest;

        /// <summary>
        ///     Gets a read-only list of the points associated with this operation.
        /// </summary>
        public new global::System.Collections.Generic.IReadOnlyList<global::Robotmaster.Processor.Estun.Generated.Interfaces.IPoint> Points
        {
            get
            {
                if (this.points == null)
                {
                    this.points = global::Robotmaster.Processor.Core.Common.BaseClasses.OperationBase.CreatePointList<global::Robotmaster.Processor.Estun.Generated.Interfaces.IOperation, global::Robotmaster.Processor.Estun.Generated.Interfaces.IPoint>(this, global::Robotmaster.Processor.Estun.Generated.Classes.Point.CreateInstance);
                }

                return this.points;
            }
        }

        /// <summary>
        ///     Gets the first point of this operation.
        /// </summary>
        public new global::Robotmaster.Processor.Estun.Generated.Interfaces.IPoint FirstPoint => (global::Robotmaster.Processor.Estun.Generated.Interfaces.IPoint)base.FirstPoint;

        /// <summary>
        ///     Gets the last point of this operation.
        /// </summary>
        public new global::Robotmaster.Processor.Estun.Generated.Interfaces.IPoint LastPoint => (global::Robotmaster.Processor.Estun.Generated.Interfaces.IPoint)base.LastPoint;

        /// <summary>
        ///     Gets the set of menus available to this operation.
        /// </summary>
        public global::Robotmaster.Processor.Estun.Generated.Artifacts.Menus.Interfaces.IOperationMenus Menus
        {
            get
            {
                if (this.operationMenus == null)
                {
                    this.operationMenus = global::Robotmaster.Processor.Estun.Generated.Artifacts.Menus.Classes.OperationMenus.CreateInstance(this);
                }

                return this.operationMenus;
            }
        }

        /// <summary>
        ///     This factory method is used to be able to generate instances of this class.
        /// </summary>
        /// <param name="program">
        ///     The program that this operation is associated with.
        /// </param>
        /// <param name="operationNode">
        ///     The underlying CBT node for this operation.
        /// </param>
        /// <param name="operationListIndex">
        ///     The index of this operation in the parent program's operation list.
        /// </param>
        /// <returns>
        ///     This returns a newly created instance.
        /// </returns>
        internal static global::Robotmaster.Processor.Estun.Generated.Interfaces.IHomeOperation CreateInstance(global::Robotmaster.Processor.Estun.Generated.Interfaces.IProgram program, global::Robotmaster.ComponentBasedTree.CbtNode operationNode, global::System.Int32 operationListIndex) => new global::Robotmaster.Processor.Estun.Generated.Classes.HomeOperation(program, operationNode, operationListIndex);

        /// <summary>
        ///     Gets the underlying read-only list of points for this operation.
        /// </summary>
        /// <remarks>
        ///     Due to the way that covariance works in generics, in order to be able to get the same collection to be accessed to both Core and the Brand Processor, the collection needs to be specified here.  Once done so, it can then be used everywhere.
        /// </remarks>
        /// <returns>
        ///     The read-only list of points.
        /// </returns>
        protected override global::System.Collections.Generic.IReadOnlyList<global::Robotmaster.Processor.Core.Common.Interfaces.IPointCore> GetPointList() => this.Points;

        /// <summary>
        ///     This is used to create a concrete operation points of interest object.
        /// </summary>
        /// <returns>
        ///     This returns a new operation points of interest object.
        /// </returns>
        protected override global::Robotmaster.Processor.Core.Common.Interfaces.IOperationPointsOfInterestCore CreateOperationPointsOfInterest() => global::Robotmaster.Processor.Estun.Generated.Classes.OperationPointsOfInterest.CreateInstance(this);
    }
}
