//-----------------------------------------------------------------------------------
// <auto-generated>
//     This file was generated by the Robotmaster Processor Definition Editor (PDE).
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//-----------------------------------------------------------------------------------

namespace Robotmaster.Processor.Estun.Generated.Classes
{
    /// <summary>
    ///     This is a concrete implementation of the generated program interface.
    /// </summary>
    [global::Robotmaster.Processor.Core.Common.Attributes.DslVersionAttribute(3)]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Robotmaster.ProcessorDefinitionEditor.GenerationServices", "7.5.12424.0")]
    internal sealed class Program : global::Robotmaster.Processor.Core.Common.BaseClasses.ProgramBase, global::Robotmaster.Processor.Estun.Generated.Interfaces.IProgram
    {
        private global::System.Collections.Generic.IReadOnlyList<global::Robotmaster.Processor.Estun.Generated.Interfaces.IOperation> operations = null;

        private Program(global::Robotmaster.Processor.Estun.Generated.Interfaces.ISetup setup, global::Robotmaster.ComponentBasedTree.CbtNode programNode)
            : base(setup, programNode)
        {
        }

        /// <summary>
        ///     Gets a read-only list of the operations associated with this program.
        /// </summary>
        public new global::System.Collections.Generic.IReadOnlyList<global::Robotmaster.Processor.Estun.Generated.Interfaces.IOperation> Operations
        {
            get
            {
                if (this.operations == null)
                {
                    var operationCreationFuncDictionary = new global::System.Collections.Generic.Dictionary<global::Robotmaster.Processor.Core.Common.Enums.OperationType, global::System.Func<global::Robotmaster.Processor.Estun.Generated.Interfaces.IProgram, global::Robotmaster.ComponentBasedTree.CbtNode, global::System.Int32, global::Robotmaster.Processor.Estun.Generated.Interfaces.IOperation>>()
                    {
                        {global::Robotmaster.Processor.Core.Common.Enums.OperationType.HomeOperation, global::Robotmaster.Processor.Estun.Generated.Classes.HomeOperation.CreateInstance},
                        {global::Robotmaster.Processor.Core.Common.Enums.OperationType.MacroOperation, global::Robotmaster.Processor.Estun.Generated.Classes.MacroOperation.CreateInstance},
                        {global::Robotmaster.Processor.Core.Common.Enums.OperationType.TaskOperation, global::Robotmaster.Processor.Estun.Generated.Classes.TaskOperation.CreateInstance},
                        {global::Robotmaster.Processor.Core.Common.Enums.OperationType.TransitionOperation, global::Robotmaster.Processor.Estun.Generated.Classes.TransitionOperation.CreateInstance},
                    };

                    this.operations = global::Robotmaster.Processor.Core.Common.BaseClasses.ProgramBase.CreateOperationList<global::Robotmaster.Processor.Estun.Generated.Interfaces.IProgram, global::Robotmaster.Processor.Estun.Generated.Interfaces.IOperation>(this, operationCreationFuncDictionary);
                }

                return this.operations;
            }
        }

        /// <summary>
        ///     Gets the first operation of this program.
        /// </summary>
        public new global::Robotmaster.Processor.Estun.Generated.Interfaces.IOperation FirstOperation => (global::Robotmaster.Processor.Estun.Generated.Interfaces.IOperation)base.FirstOperation;

        /// <summary>
        ///     Gets the last operation of this program.
        /// </summary>
        public new global::Robotmaster.Processor.Estun.Generated.Interfaces.IOperation LastOperation => (global::Robotmaster.Processor.Estun.Generated.Interfaces.IOperation)base.LastOperation;

        /// <summary>
        ///     This factory method is used to be able to generate instances of this class.
        /// </summary>
        /// <param name="setup">
        ///     The setup that this program belongs to.
        /// </param>
        /// <param name="programNode">
        ///     The underlying CBT node for this program.
        /// </param>
        /// <returns>
        ///     This returns a newly created instance.
        /// </returns>
        internal static global::Robotmaster.Processor.Estun.Generated.Interfaces.IProgram CreateInstance(global::Robotmaster.Processor.Estun.Generated.Interfaces.ISetup setup, global::Robotmaster.ComponentBasedTree.CbtNode programNode) => new global::Robotmaster.Processor.Estun.Generated.Classes.Program(setup, programNode);

        /// <summary>
        ///     Gets the underlying read-only list of operations for this program.
        /// </summary>
        /// <remarks>
        ///     Due to the way that covariance works in generics, in order to be able to get the same collection to be accessed to both Core and the Brand Processor, the collection needs to be specified here.  Once done so, it can then be used everywhere.
        /// </remarks>
        /// <returns>
        ///     The read-only list of operations.
        /// </returns>
        protected override global::System.Collections.Generic.IReadOnlyList<global::Robotmaster.Processor.Core.Common.Interfaces.IOperationCore> GetOperationList() => this.Operations;
    }
}
