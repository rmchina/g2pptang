﻿namespace Robotmaster.Processor.Estun
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Robotmaster.Processor.Core.Common.Enums;
    using Robotmaster.Processor.Core.Common.Extensions.Pdk;
    using Robotmaster.Processor.Estun.Events;
    using Robotmaster.Processor.Estun.Generated.Artifacts.Menus.Enums.ProcessActivationSettings;
    using Robotmaster.Processor.Estun.Generated.Artifacts.Processes;
    using Robotmaster.Processor.Estun.Generated.Interfaces;
    using Robotmaster.Rise.Event;
    internal class MainProcessor
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="MainProcessor"/> class.
        /// </summary>
        /// <param name="operation">
        ///     The current operation.
        /// </param>
        /// <param name="context">
        ///     The current context.
        /// </param>
        internal MainProcessor(IOperation operation, IMainProcessorContext context)
        {
            this.Context = context;
            this.Cell = context.Cell;
            this.Setup = context.Setup;
            this.Program = context.Program;
            this.Operation = operation;
        }

        /// <summary>
        ///     Gets the current context.
        /// </summary>
        internal IMainProcessorContext Context { get; }

        /// <summary>
        ///     Gets the current cell.
        /// </summary>
        internal ICell Cell { get; }

        /// <summary>
        ///     Gets the current setup.
        /// </summary>
        internal ISetup Setup { get; }

        /// <summary>
        ///     Gets the current program.
        /// </summary>
        internal IProgram Program { get; }

        /// <summary>
        ///     Gets the current operation.
        /// </summary>
        internal IOperation Operation { get; }

        internal virtual void Run()
        {
            this.EditOperation(this.Operation);
            for (var point = this.Operation.FirstPoint; point != null; point = point.NextPoint)
            {
                this.EditPoint(this.Operation, point);
            }
        }

        internal virtual void EditOperation(IOperation operation)
        {
            // Default process activation and deactivation implementation (operation level)
            this.EditOperationProcessActivationDeactivation(operation);

            // Default tool change implementation (operation level)
            //this.EditOperationToolChange(operation);

            //// CUSTOMIZATION - Uncomment the example below
            //// Example: Add events to the first point
            // operation.FirstPoint.AddEvent(new Command() { CommandText = $"Some command" }, EventIndex.Before);
            // operation.FirstPoint.AddEvent(new Comment() { CommentText = $"Some comment" }, EventIndex.Before);
        }

        internal virtual void EditPoint(IOperation operation, IPoint point)
        {
            // Default process activation and deactivation implementation (point level)
            this.EditPointProcessActivationDeactivation(operation, point);

            //// CUSTOMIZATION - Uncomment the example below
            //// Example: Add event to sharp corner
            // if (point.Flags.IsInProcess &&
            //     point != operation.LastPoint &&
            //     point.PathDirection.AngleBetween(point.NextPoint.PathDirection) > 45)
            // {
            //     point.AddEvent(new Comment() { CommentText = $"Sharp conner detected" }, EventIndex.Before);
            // }
        }

        internal virtual void EditOperationProcessActivationDeactivation(IOperation operation)
        {
            IPoint point;
            switch (operation.Menus.ProcessActivationSettings.ProcessCondition)
            {
                case ProcessCondition.EveryProgram:
                    if (operation.Flags.IsFirstTaskOperation)
                    {
                        point = this.FindFirstProcessActivationPoint(operation);
                        this.AddProcessActivationEvent(operation, point);
                    }

                    if (operation.Flags.IsLastTaskOperation)
                    {
                        point = this.FindLastProcessDeactivationPoint(operation);
                        this.AddProcessDeactivationEvent(operation, point);
                    }

                    break;
                case ProcessCondition.EveryOperation when operation.OperationType == OperationType.TaskOperation:
                    point = this.FindFirstProcessActivationPoint(operation);
                    this.AddProcessActivationEvent(operation, point);
                    point = this.FindLastProcessDeactivationPoint(operation);
                    this.AddProcessDeactivationEvent(operation, point);
                    break;
                case ProcessCondition.EveryToolChange:
                    if (operation.OperationType == OperationType.TaskOperation
                                                           && operation.PreviousTaskOperation?.Tool.ToolNumber != operation.Tool.ToolNumber)
                    {
                        point = this.FindFirstProcessActivationPoint(operation);
                        this.AddProcessActivationEvent(operation, point);
                    }

                    if (operation.OperationType == OperationType.TaskOperation
                                                           && operation.Tool.ToolNumber != operation.NextTaskOperation?.Tool.ToolNumber)
                    {
                        point = this.FindLastProcessDeactivationPoint(operation);
                        this.AddProcessDeactivationEvent(operation, point);
                    }

                    break;
                case ProcessCondition.EveryPath:
                    // Do nothing here. See EditPointProcessActivationDeactivation ran at every point.
                    break;
                default:
                    break;
            }
        }

        internal virtual void EditPointProcessActivationDeactivation(IOperation operation, IPoint point)
        {
            if (operation.OperationType == OperationType.TaskOperation &&
                operation.Menus.ProcessActivationSettings.ProcessCondition == ProcessCondition.EveryPath)
            {
                if (this.IsProcessActivationPoint(operation, point))
                {
                    this.AddProcessActivationEvent(operation, point);
                }

                if (this.IsProcessDeactivationPoint(operation, point))
                {
                    this.AddProcessDeactivationEvent(operation, point);
                }
            }
        }

        /// <summary>
        ///     Finds the first process activation point of a <paramref name="operation"/> based on the process activation menu.
        /// </summary>
        /// <param name="operation">
        ///     The current operation.
        /// </param>
        /// <returns>The first process activation point.</returns>
        internal virtual IPoint FindFirstProcessActivationPoint(IOperation operation)
        {
            switch (operation.Menus.ProcessActivationSettings.ProcessActivationCondition)
            {
                case ProcessActivationCondition.FirstPoint:
                    return operation.FirstPoint;
                case ProcessActivationCondition.FirstNoneJointMove:
                    return operation.PointsOfInterest.VeryFirstNonJointMove;
                case ProcessActivationCondition.FirstPlunge:
                    return operation.PointsOfInterest.VeryFirstPlungeMove;
                case ProcessActivationCondition.FirstPointOfContact:
                    return operation.PointsOfInterest.VeryFirstPointOfContact;
                case ProcessActivationCondition.FirstMoveInProcess:
                    return operation.PointsOfInterest.VeryFirstMoveInProcess;
                default:
                    return null;
            }
        }

        /// <summary>
        ///     Edits last process activation point of a <paramref name="operation"/> based on the process activation menu.
        /// </summary>
        /// <param name="operation">
        ///     The current operation.
        /// </param>
        /// <returns>The last process activation point.</returns>
        internal virtual IPoint FindLastProcessDeactivationPoint(IOperation operation)
        {
            switch (operation.Menus.ProcessActivationSettings.ProcessDeactivationCondition)
            {
                case ProcessDeactivationCondition.LastPoint:
                    return operation.LastPoint;
                case ProcessDeactivationCondition.LastNoneJointMove:
                    return operation.PointsOfInterest.VeryLastNonJointMove;
                case ProcessDeactivationCondition.LastRetract:
                    return operation.PointsOfInterest.VeryLastRetractMove;
                case ProcessDeactivationCondition.LastPointOfContact:
                    return operation.PointsOfInterest.VeryLastPointOfContact;
                case ProcessDeactivationCondition.LastMoveInProcess:
                    return operation.PointsOfInterest.VeryLastMoveInProcess;
                default:
                    return null;
            }
        }

        /// <summary>
        ///     Get a value indicating whether the <paramref name="point"/>  is process activation point based on the process activation menu.
        /// </summary>
        /// <param name="operation">
        ///     The current operation.
        /// </param>
        /// <param name="point">
        ///     The current point.
        /// </param>
        /// <returns><c>true</c> if <paramref name="point" /> is a process activation point; otherwise, <c>false</c>.</returns>
        internal virtual bool IsProcessActivationPoint(IOperation operation, IPoint point)
        {
            switch (operation.Menus.ProcessActivationSettings.ProcessActivationCondition)
            {
                case ProcessActivationCondition.FirstPoint:
                    return point == operation.FirstPoint;
                case ProcessActivationCondition.FirstNoneJointMove:
                    return point.Flags.IsFirstNonJointMove;
                case ProcessActivationCondition.FirstPlunge:
                    return point.Flags.IsPlungeMove;
                case ProcessActivationCondition.FirstPointOfContact:
                    return point.Flags.IsFirstPointOfContact;
                case ProcessActivationCondition.FirstMoveInProcess:
                    return point.Flags.IsFirstMoveInProcess;
                default:
                    return false;
            }
        }

        /// <summary>
        ///     Get a value indicating whether the <paramref name="point"/> is process deactivation point based on the process activation menu.
        /// </summary>
        /// <param name="operation">
        ///     The current operation.
        /// </param>
        /// <param name="point">
        ///     The current point.
        /// </param>
        /// <returns><c>true</c> if <paramref name="point" /> is a process deactivation point; otherwise, <c>false</c>.</returns>
        internal virtual bool IsProcessDeactivationPoint(IOperation operation, IPoint point)
        {
            switch (operation.Menus.ProcessActivationSettings.ProcessDeactivationCondition)
            {
                case ProcessDeactivationCondition.LastPoint:
                    return point == operation.LastPoint;
                case ProcessDeactivationCondition.LastNoneJointMove:
                    return point.Flags.IsLastNonJointMove;
                case ProcessDeactivationCondition.LastRetract:
                    return point.Flags.IsRetractMove;
                case ProcessDeactivationCondition.LastPointOfContact:
                    return point.Flags.IsLastPointOfContact;
                case ProcessDeactivationCondition.LastMoveInProcess:
                    return point.Flags.IsLastMoveInProcess;
                default:
                    return false;
            }
        }

        internal virtual void AddProcessActivationEvent(IOperation operation, IPoint point)
        {
            //// Add the process activation event
            //// CUSTOMIZATION - The behavior below can be changed

            // Initialize the event
            // Example: CALL TOOLON
            var eventToAdd = new ProcessOn
            {
                ProcessActivationCommand = operation.Menus.ProcessActivationSettings.ProcessActivationMacro,
            };

            // Define the event index (before/after)
            EventIndex eventIndex = EventIndex.Before;
            if (operation.Menus.ProcessActivationSettings.ProcessActivationIndex == ProcessActivationIndex.After)
            {
                eventIndex = EventIndex.After;
            }

            // add the event
            point.AddEvent(eventToAdd, eventIndex);

            // Example: Add other events to the process deactivation.
            // Uncomment the lines below if needed.
            // point.AddEvent(new Command() { CommandText = $"Some other command" }, eventIndex);
            // point.AddEvent(new Comment() { CommentText = $"Some comment" }, eventIndex);
        }

        /// <summary>
        ///     Adds the process deactivation event.
        /// </summary>
        /// <param name="operation">
        ///     The operation.
        /// </param>
        /// <param name="point">
        ///     The point.
        /// </param>
        internal virtual void AddProcessDeactivationEvent(IOperation operation, IPoint point)
        {
            //// Add the process deactivation event
            //// CUSTOMIZATION - The behavior below can be changed

            // Initialize the event
            // Example: CALL TOOLOFF
            var eventToAdd = new ProcessOff
            {
                ProcessDeactivationCommand = operation.Menus.ProcessActivationSettings.ProcessDeactivationMacro,
            };

            // Define the event index (before/after)
            EventIndex eventIndex = EventIndex.Before;
            if (operation.Menus.ProcessActivationSettings.ProcessDeactivationIndex == ProcessDeactivationIndex.After)
            {
                eventIndex = EventIndex.After;
            }

            // add the event
            point.AddEvent(eventToAdd, eventIndex);

            // Example: Add other events to the process deactivation.
            // Uncomment the lines below if needed.
            // point.AddEvent(new Command() { CommandText = $"Some other command" }, eventIndex);
            // point.AddEvent(new Comment() { CommentText = $"Some comment" }, eventIndex);
        }
    }
}
