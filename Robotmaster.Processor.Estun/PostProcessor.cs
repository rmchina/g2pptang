﻿namespace Robotmaster.Processor.Estun
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Core.Common.Enums;
    using Robotmaster.Processor.Core.Common.Extensions.Pdk;
    using Robotmaster.Processor.Core.Common.Interfaces.FileFramework;
    using Robotmaster.Processor.Estun.Generated.Interfaces;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.Event;
    using Robotmaster.Math.Algebra;
    using System.Text;

    internal class PostProcessor
    {
        internal PostProcessor(IProgram program, IPostProcessorContext context)
        {
            this.Context = context;
            this.Cell = context.Cell;
            this.Setup = context.Setup;
            this.Program = program;
            this.RootDirectory = context.RootPostDirectory;
        }
        /// <summary>
        ///     Gets the current context.
        /// </summary>
        internal IPostProcessorContext Context { get; }

        /// <summary>
        ///     Gets the current cell.
        /// </summary>
        internal ICell Cell { get; }

        /// <summary>
        ///     Gets the current setup.
        /// </summary>
        internal ISetup Setup { get; }

        /// <summary>
        ///     Gets the current program.
        /// </summary>
        internal IProgram Program { get; }

        /// <summary>
        ///     Gets the root directory.
        /// </summary>
        internal IPostDirectory RootDirectory { get; }

        internal IPostDirectory ProjectDirectory { get; set; }

        internal IPostDirectory ProgramDirectory { get; set; }

        internal ITextFile GlobalFile { set; get; }

        internal ITextFile PrjGlobalFile { set; get; }

        /// <summary>
        ///     Gets or sets the current file.
        /// </summary>
        internal ITextFile CurrentFile { get; set; }

        internal ITextFile CurrentDataFile { get; set; }

        /// <summary>
        ///     Gets the current <see cref="ITextSection"/> for moves.
        /// </summary>
        internal ITextSection MoveSection => this.CurrentFile.RootSection.ChildSections["MOVES"];

        /// <summary>
        ///     Gets the current <see cref="ITextSection"/> for positions.
        /// </summary>
        internal ITextSection PositionSection => this.CurrentDataFile.RootSection.ChildSections["POSITIONS"];

        /// <summary>
        ///     Gets or sets the current joint speed.
        /// </summary>
        internal IDoublePostVariable JointSpeed { get; set; }

        /// <summary>
        ///     Gets or sets the current feedrate.
        /// </summary>
        internal IDoublePostVariable Feedrate { get; set; }

        /// <summary>
        ///     Gets or sets a generic numerical values.
        /// </summary>
        internal IDoublePostVariable GenericVariable { get; set; }

        /// <summary>
        ///     Gets or sets a generic length values.
        /// </summary>
        internal IDoublePostVariable GenericLengthVariable { get; set; }

        /// <summary>
        ///     Gets or sets a generic angle values.
        /// </summary>
        internal IDoublePostVariable GenericAngleVariable { get; set; }

        /// <summary>
        ///     Gets or sets the current point number.
        /// </summary>
        internal IIntegerPostVariable PointNumber { get; set; }

        /// <summary>
        ///     Gets or sets the current line number.
        /// </summary>
        internal IIntegerPostVariable LineNumber { get; set; }

        /// <summary>
        ///     Gets or sets the current line number in the main file.
        /// </summary>
        internal IIntegerPostVariable MainFileLineNumber { get; set; }

        internal IIntegerPostVariable SubProgramFileNumber { get; set; }

        internal virtual List<string> ToolFrameInitializerList { get; set; } = new List<string>();
        internal virtual List<string> UserFrameInitializerList { get; set; } = new List<string>();
        internal virtual List<string> SpeedVaribleList { get; set; } = new List<string>();
        internal virtual List<string> PositioningVaribleList { get; set; } = new List<string>();

        /// <summary>
        ///     This is for the post processor entry point.
        /// </summary>
        internal void Run()
        {
            this.InitializeDataBeforeProgram();
            this.OutputBeforeProgram();

            foreach (var operation in this.Program.Operations)
            {
                //this.InitializeDataBeforeOperation(operation);
                this.OutputBeforeOperation(operation);
                for (var point = operation.FirstPoint; point != null; point = point.NextPoint)
                {
                    this.InitializeDataBeforePoint(operation, point);
                    this.OutputBeforePoint(operation, point);
                    this.OutputPoint(operation, point);
                   this.OutputAfterPoint(operation, point);
                }
                //this.OutputAfterOperation(operation);
            }

            //this.OutputAfterProgram();
            //this.FinalizeDataAfterProgram();
           this.GenerateFiles();
        }

        internal virtual void InitializeDataBeforeProgram()
        {
            this.InitializeVariables();
            //this.InitializeFanucGroups();
            this.InitializeFileStructure();
        }

        internal virtual void InitializeVariables()
        {
            this.JointSpeed = this.Context.CreateDoublePostVariable(0, " {0:0.##} %");
            this.Feedrate = this.Context.CreateDoublePostVariable(0, " {0:0.###} mm/sec");
            this.PointNumber = this.Context.CreateIntegerPostVariable(0, "P{0}");
            this.LineNumber = this.Context.CreateIntegerPostVariable(0, "{0,0:0}0:");
            this.MainFileLineNumber = this.Context.CreateIntegerPostVariable(0, "{0,4:0}: ");

            this.SubProgramFileNumber = this.Context.CreateIntegerPostVariable(0, "{0}");


            this.GenericVariable = this.Context.CreateDoublePostVariable(0, "{0:0.###}", false);
            this.GenericLengthVariable = this.Context.CreateDoublePostVariable(0, "{0,8:0.000}  mm", false);
            this.GenericAngleVariable = this.Context.CreateDoublePostVariable(0, "{0,8:0.000} deg", false);
        }

        internal virtual void InitializeFileStructure()
        {
            var firstOperation = this.Program.FirstOperation;
            var firstPoint = firstOperation.FirstPoint;

            if (((int)firstOperation.Menus.ControllerSettings.SplitLongFileBy) == 0)
            {
                if (firstOperation.Menus.ControllerSettings.MaximumPointNumber <= 0)
                {

                    this.ProjectDirectory = this.RootDirectory.CreateChildDirectory("Project");
                    this.GlobalFile = this.ProjectDirectory.CreateChildTextFile("_global", ".erd");
                    this.ProgramDirectory = this.ProjectDirectory.CreateChildDirectory(this.Program.Name.Replace(" ", "_") + ".er");
                    Directory.Delete(this.ProgramDirectory.AbsolutePath, true);
                    this.ProgramDirectory = this.ProjectDirectory.CreateChildDirectory(this.Program.Name.Replace(" ", "_") + ".er");

                    this.CurrentFile = this.ProgramDirectory.CreateChildTextFile(this.Program.Name.Replace(" ", "_"), ".erp");
                    this.CurrentDataFile = this.ProgramDirectory.CreateChildTextFile(this.Program.Name.Replace(" ", "_"), ".erd");
                    this.PrjGlobalFile = this.ProgramDirectory.CreateChildTextFile("prjglobal", ".erd");

                    this.StartSrpFile(this.CurrentFile, firstOperation, firstPoint);
                    this.StartSrdFile(this.CurrentDataFile, firstOperation, firstPoint);
                }
                else
                {
                    this.ProjectDirectory = this.RootDirectory.CreateChildDirectory("Project");
                    this.GlobalFile = this.ProjectDirectory.CreateChildTextFile("_global", ".erd");
                    this.ProgramDirectory = this.ProjectDirectory.CreateChildDirectory(this.Program.Name.Replace(" ", "_") + ".er");
                    Directory.Delete(this.ProgramDirectory.AbsolutePath, true);
                    this.ProgramDirectory = this.ProjectDirectory.CreateChildDirectory(this.Program.Name.Replace(" ", "_") + ".er");

                    var mainFile = this.ProgramDirectory.CreateChildTextFile(this.Program.Name.Replace(" ", "_").ToUpper(), ".erp");
                    this.ProgramDirectory.CreateChildTextFile(this.Program.Name.Replace(" ", "_").ToUpper(), ".erd");
                    this.StartMainFile(mainFile);

                    var currentPostFileName = this.FindNextAvailableChildName(mainFile, firstOperation, firstOperation.FirstPoint);
                    this.CurrentFile = this.ProgramDirectory.CreateChildTextFile(currentPostFileName.ToUpper(), ".erp", mainFile);
                    this.CurrentDataFile = this.ProgramDirectory.CreateChildTextFile(currentPostFileName.ToUpper(), ".erd", mainFile);
                    this.PrjGlobalFile = this.ProgramDirectory.CreateChildTextFile("prjglobal", ".erd");
                    this.CallFileIntoParent(this.CurrentFile, firstOperation, firstOperation.FirstPoint);
                    this.StartSrpFile(this.CurrentFile, firstOperation, firstPoint);
                    this.StartSrdFile(this.CurrentDataFile, firstOperation, firstPoint);
                }
            }
            if (((int)firstOperation.Menus.ControllerSettings.SplitLongFileBy) == 1)
            {
                if (firstOperation.Menus.ControllerSettings.MaximumLineNumber <= 0)
                {

                    this.ProjectDirectory = this.RootDirectory.CreateChildDirectory("Project");
                    this.GlobalFile = this.ProjectDirectory.CreateChildTextFile("_global", ".erd");
                    this.ProgramDirectory = this.ProjectDirectory.CreateChildDirectory(this.Program.Name.Replace(" ", "_") + ".er");
                    Directory.Delete(this.ProgramDirectory.AbsolutePath, true);
                    this.ProgramDirectory = this.ProjectDirectory.CreateChildDirectory(this.Program.Name.Replace(" ", "_") + ".er");

                    this.CurrentFile = this.ProgramDirectory.CreateChildTextFile(this.Program.Name.Replace(" ", "_"), ".erp");
                    this.CurrentDataFile = this.ProgramDirectory.CreateChildTextFile(this.Program.Name.Replace(" ", "_"), ".erd");
                    this.PrjGlobalFile = this.ProgramDirectory.CreateChildTextFile("prjglobal", ".erd");

                    this.StartSrpFile(this.CurrentFile, firstOperation, firstPoint);
                    this.StartSrdFile(this.CurrentDataFile, firstOperation, firstPoint);
                }
                else
                {
                    this.ProjectDirectory = this.RootDirectory.CreateChildDirectory("Project");
                    this.GlobalFile = this.ProjectDirectory.CreateChildTextFile("_global", ".erd");
                    this.ProgramDirectory = this.ProjectDirectory.CreateChildDirectory(this.Program.Name.Replace(" ", "_") + ".er");
                    Directory.Delete(this.ProgramDirectory.AbsolutePath,true);
                    this.ProgramDirectory = this.ProjectDirectory.CreateChildDirectory(this.Program.Name.Replace(" ", "_") + ".er");

                    var mainFile = this.ProgramDirectory.CreateChildTextFile(this.Program.Name.Replace(" ", "_").ToUpper(), ".erp");
                    this.ProgramDirectory.CreateChildTextFile(this.Program.Name.Replace(" ", "_").ToUpper(), ".erd");
                    this.StartMainFile(mainFile);

                    var currentPostFileName = this.FindNextAvailableChildName(mainFile, firstOperation, firstOperation.FirstPoint);
                    this.CurrentFile = this.ProgramDirectory.CreateChildTextFile(currentPostFileName.ToUpper(), ".erp", mainFile);
                    this.CurrentDataFile = this.ProgramDirectory.CreateChildTextFile(currentPostFileName.ToUpper(), ".erd", mainFile);
                    this.PrjGlobalFile = this.ProgramDirectory.CreateChildTextFile("prjglobal", ".erd");
                    this.CallFileIntoParent(this.CurrentFile, firstOperation, firstOperation.FirstPoint);
                    this.StartSrpFile(this.CurrentFile, firstOperation, firstPoint);
                    this.StartSrdFile(this.CurrentDataFile, firstOperation, firstPoint);
                }
            }

            //if (firstOperation.Menus.ControllerSettings.MaximumLineNumber <= 0)
            //{

            //    this.ProjectDirectory = this.RootDirectory.CreateChildDirectory("Project");
            //    this.GlobalFile = this.ProjectDirectory.CreateChildTextFile("_global", ".erd");
            //    this.ProgramDirectory = this.ProjectDirectory.CreateChildDirectory(this.Program.Name.Replace(" ", "_") + ".er");

            //    this.CurrentFile = this.ProgramDirectory.CreateChildTextFile(this.Program.Name.Replace(" ", "_"), ".erp");
            //    this.CurrentDataFile = this.ProgramDirectory.CreateChildTextFile(this.Program.Name.Replace(" ", "_"), ".erd");
            //    this.PrjGlobalFile = this.ProgramDirectory.CreateChildTextFile("prjglobal", ".erd");

            //    this.StartSrpFile(this.CurrentFile, firstOperation, firstPoint);
            //    this.StartSrdFile(this.CurrentDataFile, firstOperation, firstPoint);
            //}
            //else
            //{
            //    this.ProjectDirectory = this.RootDirectory.CreateChildDirectory("Project");
            //    this.GlobalFile = this.ProjectDirectory.CreateChildTextFile("_global", ".erd");
            //    this.ProgramDirectory = this.ProjectDirectory.CreateChildDirectory(this.Program.Name.Replace(" ", "_") + ".er");

            //    var mainFile = this.ProgramDirectory.CreateChildTextFile(this.Program.Name.Replace(" ", "_").ToUpper(), ".erp");
            //    this.ProgramDirectory.CreateChildTextFile(this.Program.Name.Replace(" ", "_").ToUpper(), ".erd");
            //    this.StartMainFile(mainFile);

            //    var currentPostFileName = this.FindNextAvailableChildName(mainFile, firstOperation, firstOperation.FirstPoint);
            //    this.CurrentFile = this.ProgramDirectory.CreateChildTextFile(currentPostFileName.ToUpper(), ".erp", mainFile);
            //    this.CurrentDataFile = this.ProgramDirectory.CreateChildTextFile(currentPostFileName.ToUpper(), ".erd", mainFile);
            //    this.PrjGlobalFile = this.ProgramDirectory.CreateChildTextFile("prjglobal", ".erd");
            //    this.CallFileIntoParent(this.CurrentFile, firstOperation, firstOperation.FirstPoint);
            //    this.StartSrpFile(this.CurrentFile, firstOperation, firstPoint);
            //    this.StartSrdFile(this.CurrentDataFile, firstOperation, firstPoint);
            //}
        }

        internal virtual void StartMainFile(ITextFile mainFile)
        {
            var callSubProgramSection = mainFile.RootSection.CreateChildSection("CALLSUBPRGS");
            //this.WriteMainFileHeader(mainFile);
            mainFile.RootSection.Header.WriteLine("// InstParseVersion: V1.02");
            mainFile.RootSection.Header.WriteLine("Start:");
            mainFile.RootSection.Footer.WriteLine("End;");
        }

        internal virtual string FindNextAvailableChildName(ITextFile file, IOperation operation, IPoint point)
        {
            this.SubProgramFileNumber.Increment();
            string subPostFileNamePattern = file.FileNameWithoutExtension + "_";
            return subPostFileNamePattern + SubProgramFileNumber;
        }

        internal virtual void CallFileIntoParent(ITextFile file, IOperation operation, IPoint point)
        {
            file.Parent.RootSection.ChildSections["CALLSUBPRGS"]
                .WriteLine($"CALL {file.FileNameWithoutExtension}");
        }

        internal virtual void StartSrpFile(ITextFile file, IOperation operation, IPoint point)
        {
            var movesSection = file.RootSection.CreateChildSection("MOVES");
            this.LineNumber.Increment();
            file.RootSection.Header.WriteLine("// InstParseVersion: V1.02");
            this.LineNumber.Increment();
            file.RootSection.Header.WriteLine("/*PROGRAMMED BY ROBOTMASTER*/");
            this.LineNumber.Increment();
            MoveSection.Header.WriteLine("Start:");
            this.LineNumber.Increment();
            MoveSection.Footer.WriteLine("End;");
        }
        internal virtual void StartSrdFile(ITextFile file, IOperation operation, IPoint point)
        {
            var positionSection = file.RootSection.CreateChildSection("POSITIONS");
            //file.RootSection.Header.WriteLine("/*PROGRAMMED BY ROBOTMASTER*/");
        }

        internal virtual void InitializeDataBeforePoint(IOperation operation, IPoint point)
        {
            this.UpdateFileStructure(operation, point);
        }

        internal virtual void UpdateFileStructure(IOperation operation, IPoint point)
        {
            if (((int)operation.Menus.ControllerSettings.SplitLongFileBy) == 0)
            {
                if (operation.Menus.ControllerSettings.MaximumPointNumber > 0
                && this.PointNumber.Value >= operation.Menus.ControllerSettings.MaximumPointNumber
                && (point.MotionType != PointMotionType.Circular || point.IsArcMiddlePoint))
                {
                    this.EndFile(this.CurrentFile, operation, point);
                    var currentPostFileName = this.FindNextAvailableChildName(this.CurrentFile.Parent, operation, point);
                    this.CurrentFile = this.ProgramDirectory.CreateChildTextFile(currentPostFileName, ".erp", this.CurrentFile.Parent);
                    this.CurrentDataFile = this.ProgramDirectory.CreateChildTextFile(currentPostFileName, ".erd", this.CurrentFile.Parent);
                    this.StartSrpFile(this.CurrentFile, operation, point);
                    this.StartSrdFile(this.CurrentDataFile, operation, point);
                    this.CallFileIntoParent(this.CurrentFile, operation, point);
                }
            }
            else if(((int)operation.Menus.ControllerSettings.SplitLongFileBy) == 1)
            {
                if (operation.Menus.ControllerSettings.MaximumLineNumber > 0
                && this.LineNumber.Value >= operation.Menus.ControllerSettings.MaximumLineNumber
                && (point.MotionType != PointMotionType.Circular || point.IsArcMiddlePoint))
                {
                    this.EndFile(this.CurrentFile, operation, point);
                    var currentPostFileName = this.FindNextAvailableChildName(this.CurrentFile.Parent, operation, point);
                    this.CurrentFile = this.ProgramDirectory.CreateChildTextFile(currentPostFileName, ".erp", this.CurrentFile.Parent);
                    this.CurrentDataFile = this.ProgramDirectory.CreateChildTextFile(currentPostFileName, ".erd", this.CurrentFile.Parent);
                    this.StartSrpFile(this.CurrentFile, operation, point);
                    this.StartSrdFile(this.CurrentDataFile, operation, point);
                    this.CallFileIntoParent(this.CurrentFile, operation, point);
                }
            }

        }

        internal virtual void EndFile(ITextFile file, IOperation operation, IPoint point)
        {
            this.PointNumber.Value = 0;
            this.LineNumber.Value = 0;
        }

        internal virtual void OutputBeforeProgram()
        {
        }



        internal virtual void OutputBeforeOperation(IOperation operation)
        {
            this.LineNumber.Increment();
            MoveSection.WriteLine($"/*{operation.Name}*/");
            //SetTool{ Tool = t_g.TOOL0}
            //SetCoord{ Coord = t_g.USERCOOR0}
            //TOOL1 ={ _type = "TOOL",id = 2,x = 175.889,y = 0.000 ,z = 234.388 ,a = 180.000,b = -60.001,c = 0.000}
            //USERCOOR1 ={ _type = "USERCOOR",id = 2,x = 1000.000,y = 0.000 ,z = 500.000 ,a = 0.000,b = 0.000,c = 0.000}
            //输出当前激活工具和用户坐标系
            if (operation.UserFrame.Number != operation.PreviousOperation?.UserFrame.Number)
            {
                this.OutputUserFrame(operation);
            }
            if (operation.TCPFrame.Number != operation.PreviousOperation?.TCPFrame.Number)
            {
                this.OutputToolFrame(operation);
            }
            //V200 ={ _type = "SPEED",per = 100.000000,tcp = 4000.000000,ori = 360.000000,exj_l = 360.000000,exj_r = 180.000000}
            //C100 ={ _type = "ZONE",per = 0.000000,dis = 0.000000,vConst = 0}
            if (!this.SpeedVaribleList.Contains(operation.Menus.EstunMotionSettings.JointSpeedVarible))
            {
                this.PrjGlobalFile.RootSection.WriteLine($"{operation.Menus.EstunMotionSettings.JointSpeedVarible}" +
                    $"={{_type=\"SPEED\",per={operation.Menus.EstunMotionSettings.JointSpeedPer,0:0.000}," +
                    $"tcp={operation.Menus.EstunMotionSettings.JointSeepdTCP,0:0.000}," +
                    $"ori={operation.Menus.EstunMotionSettings.JointSeepdOri,0:0.000}," +
                    $"exj_l={operation.Menus.EstunMotionSettings.JointSeepdExjl,0:0.000}," +
                    $"exj_r={operation.Menus.EstunMotionSettings.JointSeepdExjr,0:0.000}}}");
                this.SpeedVaribleList.Add(operation.Menus.EstunMotionSettings.JointSpeedVarible);
            }
            if (!this.PositioningVaribleList.Contains(operation.Menus.EstunMotionSettings.JointPositioningVarible))
            {
                this.PrjGlobalFile.RootSection.WriteLine($"{operation.Menus.EstunMotionSettings.JointPositioningVarible}" +
                    $"={{_type=\"ZONE\",per={operation.Menus.EstunMotionSettings.JointPositioningPer,0:0.000}," +
                    $"dis={operation.Menus.EstunMotionSettings.JointPositioningDis,0:0.000}," +
                    $"vConst={operation.Menus.EstunMotionSettings.JointPositioningvConst,0:0.000}}}");
                this.PositioningVaribleList.Add(operation.Menus.EstunMotionSettings.LinearAndCircularPositioningVarible);
            }
            if (!this.SpeedVaribleList.Contains(operation.Menus.EstunMotionSettings.LinearAndCircularSpeedVarible))
            {
                this.PrjGlobalFile.RootSection.WriteLine($"{operation.Menus.EstunMotionSettings.LinearAndCircularSpeedVarible}" +
                    $"={{_type=\"SPEED\",per={operation.Menus.EstunMotionSettings.LinearAndCircularSpeedPer,0:0.000}," +
                    $"tcp={operation.Menus.EstunMotionSettings.LinearAndCircularSeepdTCP,0:0.000}," +
                    $"ori={operation.Menus.EstunMotionSettings.LinearAndCircularSeepdOri,0:0.000}," +
                    $"exj_l={operation.Menus.EstunMotionSettings.LinearAndCircularSeepdExjl,0:0.000}," +
                    $"exj_r={operation.Menus.EstunMotionSettings.LinearAndCircularSeepdExjr,0:0.000}}}");
                this.SpeedVaribleList.Add(operation.Menus.EstunMotionSettings.LinearAndCircularSpeedVarible);
            }
            if (!this.PositioningVaribleList.Contains(operation.Menus.EstunMotionSettings.LinearAndCircularPositioningVarible))
            {
                this.PrjGlobalFile.RootSection.WriteLine($"{operation.Menus.EstunMotionSettings.LinearAndCircularPositioningVarible}" +
                    $"={{_type=\"ZONE\",per={operation.Menus.EstunMotionSettings.LinearAndCircularPositioningPer,0:0.000}," +
                    $"dis={operation.Menus.EstunMotionSettings.LinearAndCircularPositioningDis,0:0.000}," +
                    $"vConst={operation.Menus.EstunMotionSettings.LinearAndCircularPositioningvConst,0:0.000}}}");
                this.PositioningVaribleList.Add(operation.Menus.EstunMotionSettings.LinearAndCircularPositioningVarible);
            }
        }

        internal virtual void OutputUserFrame(IOperation operation)
        {
            if (!this.Setup.Configuration.IsRTCP)
            {
                //SetTool{ Tool = t_g.TOOL0}
                //SetCoord{ Coord = t_g.USERCOOR0}
                this.LineNumber.Increment();
                MoveSection.WriteLine($"SetCoord{{Coord=t_g.USERCOOR{operation.UserFrame.Number}}}");
                //USERCOOR1 ={ _type = "USERCOOR",id = 2,x = 1000.000,y = 0.000 ,z = 500.000 ,a = 0.000,b = 0.000,c = 0.000}
                //USERCOOR0={_type="USERCOOR",id=1,x=1220.312900,y=-10.691900,z=815.339800,a=46.734970,b=1.601078,c=-178.281281}
                string userFrameDeclaration = string.Empty;
                userFrameDeclaration = string.Format("USERCOOR{0}={{_type=\"USERCOOR\",id={0},x={1,0:0.000},y={2,0:0.000},z={3,0:0.000},a={4,0:0.000},b={5,0:0.000},c={6,0:0.000}}}",
                    operation.UserFrame.Number,
                    operation.UserFrame.Position.X,
                    operation.UserFrame.Position.Y,
                    operation.UserFrame.Position.Z,
                    operation.UserFrame.OrientationEuler.X,
                    operation.UserFrame.OrientationEuler.Y,
                    operation.UserFrame.OrientationEuler.Z);
                if (!UserFrameInitializerList.Contains(userFrameDeclaration))
                {
                    GlobalFile.RootSection.WriteLine(userFrameDeclaration);
                    UserFrameInitializerList.Add(userFrameDeclaration);
                }
            }
        }

        internal virtual void OutputToolFrame(IOperation operation)
        {
            if (!this.Setup.Configuration.IsRTCP)
            {
                this.LineNumber.Increment();
                MoveSection.WriteLine($"SetTool{{Tool=t_g.TOOL{operation.TCPFrame.Number}}}");
                //TOOL0 ={ _type = "TOOL",id = 1,x = -36.392658,y = 70.270382,z = 345.669183,a = 0.000000,b = 0.000000,c = 0.000000,dyn ={ _type = "LoadDyn",M = 0.000000,pos ={ _type = "CenterPos",Mx = 0.000000,My = 0.000000,Mz = 0.000000},tensor ={ _type = "InertiaTensor",Ixx = 0.000000,Iyy = 0.000000,Izz = 0.000000,Ixy = 0.000000,Iyz = 0.000000,Ixz = 0.000000} } }
                //TOOL1 ={ _type = "TOOL",id = 2,x = 175.889,y = 0.000 ,z = 234.388 ,a = 180.000,b = -60.001,c = 0.000}
                string toolFrameDeclaration = string.Empty;
                toolFrameDeclaration = string.Format("TOOL{0}={{_type=\"TOOL\",id={0},x={1,0:0.000},y={2,0:0.000},z={3,0:0.000},a={4,0:0.000},b={5,0:0.000},c={6,0:0.000},dyn={{_type =\"LoadDyn\",M=0.000000,pos={{_type =\"CenterPos\",Mx=0.000000,My=0.000000,Mz=0.000000}},tensor={{_type=\"InertiaTensor\",Ixx=0.000000,Iyy=0.000000,Izz=0.000000,Ixy=0.000000,Iyz=0.000000,Ixz=0.000000}}}}}}",
                    operation.TCPFrame.Number,
                    operation.TCPFrame.Position.X,
                    operation.TCPFrame.Position.Y,
                    operation.TCPFrame.Position.Z,
                    operation.TCPFrame.OrientationEuler.X,
                    operation.TCPFrame.OrientationEuler.Y,
                    operation.TCPFrame.OrientationEuler.Z);
                if (!ToolFrameInitializerList.Contains(toolFrameDeclaration))
                {
                    GlobalFile.RootSection.WriteLine(toolFrameDeclaration);
                    ToolFrameInitializerList.Add(toolFrameDeclaration);
                }
            }
        }

        internal virtual void OutputPoint(IOperation operation, IPoint point)
        {
            switch (point.MotionSpace)
            {
                case PointMotionSpace.JointSpace:
                    this.OutputJointSpaceMove(operation, point);
                    break;
                case PointMotionSpace.CartesianSpace:
                    switch (point.MotionType)
                    {
                        case PointMotionType.Joint:
                            this.OutputJointMove(operation, point);
                            break;
                        case PointMotionType.Linear:
                            this.OutputLinearMove(operation, point);
                            break;
                        case PointMotionType.Circular when point.IsArcMiddlePoint:
                            // wait for endpoint
                            break;
                        case PointMotionType.Circular when !point.IsArcMiddlePoint:
                            this.OutputCircularMove(operation, point.PreviousPoint, point);
                            break;
                        default:
                            throw new NotImplementedException();
                    }
                    break;
            }
        }

        internal virtual void OutputJointSpaceMove(IOperation operation, IPoint point)
        {
            //P0 ={ _type = "APOS",a1 = -25.424100,a2 = 8.199400,a3 = -11.348500,a4 = 0.302700,a5 = 78.191000,a6 = 204.310600,a7 = 0.000000,a8 = 0.000000,a9 = 0.000000,a10 = 0.000000,a11 = 0.000000,a12 = 0.000000,a13 = 0.000000,a14 = 0.000000,a15 = 0.000000,a16 = 0.000000}
            //P1 ={ _type = "CPOS",confdata ={ _type = "POSCFG",mode = 0,cf1 = 0,cf2 = 0,cf3 = 0,cf4 = 0,cf5 = 0,cf6 = 1},x = -26.814200,y = -120.866300,z = -290.381800,a = -3.243000,b = 7.668200,c = -37.665300,a7 = 0.000000,a8 = 0.000000,a9 = 0.000000,a10 = 0.000000,a11 = 0.000000,a12 = 0.000000,a13 = 0.000000,a14 = 0.000000,a15 = 0.000000,a16 = 0.000000}
            //MovL{ P = t_l.P1,V = t_s.V2000,B = "RELATIVE",C = t_s.C100}
            //MovJ{ P = t_l.P0,DO = ""}
            this.PointNumber.Increment();
            this.LineNumber.Increment();
            string PositioningMethod = (operation.Menus.EstunMotionSettings.JointPositioningType == 0) ? "}" : $",C=t_p.{operation.Menus.EstunMotionSettings.JointPositioningVarible}}}";
            MoveSection.WriteLine($"MovJ{{P=t_l.{this.PointNumber},V=t_p.{operation.Menus.EstunMotionSettings.JointSpeedVarible},B=\"{operation.Menus.EstunMotionSettings.JointPositioningType}\"{PositioningMethod}");
            PositionSection.WriteLine($"{this.PointNumber}={{_type=\"APOS\",{this.FormatRobotJointValues(operation, point)},a7=0.000000,a8=0.000000,a9=0.000000,a10=0.000000,a11=0.000000,a12=0.000000,a13=0.000000,a14=0.000000,a15 =0.000000,a16=0.000000}}");
        }

        internal virtual void OutputJointMove(IOperation operation, IPoint point)
        {
            //120 MOV P2
            this.PointNumber.Increment();
            this.LineNumber.Increment();
            string PositioningMethod = (operation.Menus.EstunMotionSettings.JointPositioningType == 0) ? "}" : $",C=t_p.{operation.Menus.EstunMotionSettings.JointPositioningVarible}}}";
            MoveSection.WriteLine($"MovJ{{P=t_l.{this.PointNumber},V=t_p.{operation.Menus.EstunMotionSettings.JointSpeedVarible},B=\"{operation.Menus.EstunMotionSettings.JointPositioningType}\"{PositioningMethod}");
            PositionSection.WriteLine($"{this.PointNumber}={{_type=\"APOS\",{this.FormatRobotJointValues(operation, point)},a7=0.000000,a8=0.000000,a9=0.000000,a10=0.000000,a11=0.000000,a12=0.000000,a13=0.000000,a14=0.000000,a15 =0.000000,a16=0.000000}}");
        }

        internal virtual void OutputLinearMove(IOperation operation, IPoint point)
        {
            //MovL{P=t_l.P0,V=t_p.V100,B="RELATIVE",C=t_s.C100,Tool=t_g.TOOL2,Coord=t_g.USERCOOR1}
            //P1 = {_type = "CPOS",confdata ={ _type = "POSCFG",mode = 0,cf1 = 0,cf2 = 0,cf3 = 0,cf4 = 0,cf5 = 0,cf6 = 1},x = -26.814200,y = -120.866300,z = -290.381800,a = -3.243000,b = 7.668200,c = -37.665300,a7 = 0.000000,a8 = 0.000000,a9 = 0.000000,a10 = 0.000000,a11 = 0.000000,a12 = 0.000000,a13 = 0.000000,a14 = 0.000000,a15 = 0.000000,a16 = 0.000000}
            this.PointNumber.Increment();
            this.LineNumber.Increment();
            string PositioningMethod = (operation.Menus.EstunMotionSettings.LinearAndCircularPositioningType == 0) ? "}" : $",C=t_p.{operation.Menus.EstunMotionSettings.LinearAndCircularPositioningVarible}}}";
            MoveSection.WriteLine($"MovL{{P=t_l.{this.PointNumber},V=t_p.{operation.Menus.EstunMotionSettings.LinearAndCircularSpeedVarible},B=\"{operation.Menus.EstunMotionSettings.LinearAndCircularPositioningType}\"{PositioningMethod}");
            PositionSection.WriteLine($"{this.PointNumber}={{_type=\"CPOS\",confdata={{{this.FormatConfigAndMode(operation,point)}}},{this.FormatPositionAndOritation(operation,point)},a7=0.000000,a8=0.000000,a9=0.000000,a10=0.000000,a11=0.000000,a12=0.000000,a13=0.000000,a14=0.000000,a15=0.000000,a16=0.000000}}");
        }

        internal virtual void OutputCircularMove(IOperation operation, IPoint midPoint, IPoint endPoint)
        {
            this.PointNumber.Increment();
            PositionSection.WriteLine($"{this.PointNumber}={{_type=\"CPOS\",confdata={{{this.FormatConfigAndMode(operation, midPoint)}}},{this.FormatPositionAndOritation(operation, midPoint)},a7=0.000000,a8=0.000000,a9=0.000000,a10=0.000000,a11=0.000000,a12=0.000000,a13=0.000000,a14=0.000000,a15=0.000000,a16=0.000000}}");
            this.PointNumber.Increment();
            PositionSection.WriteLine($"{this.PointNumber}={{_type=\"CPOS\",confdata={{{this.FormatConfigAndMode(operation, endPoint)}}},{this.FormatPositionAndOritation(operation, endPoint)},a7=0.000000,a8=0.000000,a9=0.000000,a10=0.000000,a11=0.000000,a12=0.000000,a13=0.000000,a14=0.000000,a15=0.000000,a16=0.000000}}");
            string PositioningMethod = (operation.Menus.EstunMotionSettings.LinearAndCircularPositioningType == 0) ? "}" : $",C=t_p.{operation.Menus.EstunMotionSettings.LinearAndCircularPositioningVarible}}}";
            this.LineNumber.Increment();
            MoveSection.WriteLine($"MovC{{A=t_l.P{this.PointNumber.Value-1},P=t_l.{this.PointNumber},V=t_p.{operation.Menus.EstunMotionSettings.LinearAndCircularSpeedVarible},B=\"{operation.Menus.EstunMotionSettings.LinearAndCircularPositioningType}\"{PositioningMethod}");
            //2120 MVC P64, P65, P66
            
        }

        internal virtual string FormatRobotJointValues(IOperation operation, IPoint point)
        {
            //AP3 ={ _type = "APOS",a1 = 0.000,a2 = 0.000,a3 = 0.000,a4 = 0.000,a5 = 45.000,a6 = 0.000}
            //CP1 ={ _type = "CPOS",x = -38.276,y = 18.846,z = 0.000,a = 50.588,b = 180.000,c = 180.000,mode = 0}
            string output = string.Empty;
            output = string.Format("a1={0,0:0.000},a2={1,0:0.000},a3={2,0:0.000},a4={3,0:0.000},a5={4,0:0.000},a6={5,0:0.000}",
                point.GetJointValue("J1"),
                point.GetJointValue("J2"),
                point.GetJointValue("J3"),
                point.GetJointValue("J4"),
                point.GetJointValue("J5"),
                point.GetJointValue("J6")
                );
            return output;
        }

        internal virtual string FormatPositionAndOritation(IOperation operation, IPoint point)
        {
            //P3=(+577.75,+11.41,-68.73,+179.88,+1.09,-13.35)(7,1048576)
            string output = string.Empty;
            output = string.Format("x={0,0:0.000},y={1,0:0.000},z={2,0:0.000},a={3,0:0.000},b={4,0:0.000},c={5,0:0.000}",
                point.Position.X,
                point.Position.Y,
                point.Position.Z,
                point.OrientationEuler.X,
                point.OrientationEuler.Y,
                point.OrientationEuler.Z,
                0) ;
            return output;
        }

        internal virtual string FormatConfigAndMode(IOperation operation, IPoint point)
        {
            //confdata ={ mode = 0, cf1 = 0,cf2 = 0,cf3 = 0,cf4 = 0,cf5 = 0,cf6 = 1};
            string output = string.Empty;
            int cf1=this.ConfigCalculate(operation, point,1);
            int cf2 = this.ConfigCalculate(operation, point, 2);
            int cf3 = this.ConfigCalculate(operation, point, 3);
            int cf4 = this.ConfigCalculate(operation, point, 4);
            int cf5 = this.ConfigCalculate(operation, point, 5);
            int cf6 = this.ConfigCalculate(operation, point, 6);        
            output = string.Format("_type=\"POSCFG\",mode={0},cf1={1},cf2={2},cf3={3},cf4={4},cf5={5},cf6={6}",
                this.ModeCalculate(operation, point), cf1, cf2, cf3, cf4, cf5, cf6);
            //output = string.Format($"mode = {0}, " +
            //    $"cf1 = { (int)Math.Ceiling((-540- 180.0) / 360.0)}," +
            //    $" cf2 = { (int)Math.Ceiling((-180 - 180.0) / 360.0)}, " +
            //    $"cf3 = { (int)Math.Ceiling((180 - 180.0) / 360.0)}, " +
            //    $"cf4 = { (int)Math.Ceiling((540 - 180.0) / 360.0)}," +
            //    $" cf5 = { (int)Math.Ceiling((-181 - 180.0) / 360.0)}," +
            //    $" cf6 = { (int)Math.Ceiling((181 - 180.0) / 360.0)}");

            return output;
        }

        internal virtual int  ConfigCalculate(IOperation operation, IPoint point,int i)
        {
            int cfValue = 0;
            cfValue = (int)Math.Ceiling(( point.GetJointValue($"J{i}") - 180.0) / 360.0);
            return cfValue;           
        }

        internal virtual int ModeCalculate(IOperation operation, IPoint point)
        {
            int flag5= point.GetJointValue("J5")>=0?0:1;
            int flag3= point.GetJointValue("J3") >= 0? 0 : 1;
            int flag1 = 0;
            return flag5+2*flag3+4*flag1;
        }

        internal virtual void OutputBeforePoint(IOperation operation, IPoint point)
        {
            OutputBeforePointEvents(operation, point); 
        }

        internal virtual void OutputAfterPoint(IOperation operation, IPoint point)
        {
            OutputAfterPointEvents(operation, point);
        }


        internal virtual void OutputAfterPointEvents(IOperation operation, IPoint point)
        {
            List<Event> eventafterList = ((PathNode)point.CoreNode).Events()?.EventListAfter;
            if (eventafterList != null)
            {
                foreach (Event afterEvent in eventafterList)
                {
                    this.LineNumber.Increment();
                    this.MoveSection.WriteLine($"{afterEvent.ToCode()}");
                }
            }
        }

        internal virtual void OutputBeforePointEvents(IOperation operation, IPoint point)
        {
            List<Event> eventBeforeList = ((PathNode)point.CoreNode).Events()?.EventListBefore;
            if (eventBeforeList != null)
            {
                foreach (Event beforeEvent in eventBeforeList)
                {
                    this.LineNumber.Increment();
                    this.MoveSection.WriteLine($"{beforeEvent.ToCode()}");
                }
            }
        }

        internal virtual void GenerateFiles()
        {
            this.RootDirectory.GenerateFiles();
            foreach (var file in this.ProjectDirectory.ChildFiles)
            {
                //File.CreateText(file.AbsolutePath + "\\" + file.FileNameWithoutExtension + ".temp");
                string tempText = File.ReadAllText(file.AbsolutePath);
                File.WriteAllText(file.AbsolutePath, tempText);
            }
            foreach (var file in this.ProgramDirectory.ChildFiles)
            {
                //File.CreateText(file.AbsolutePath + "\\" + file.FileNameWithoutExtension + ".temp");
                string tempText = File.ReadAllText(file.AbsolutePath);
                File.WriteAllText(file.AbsolutePath, tempText);
            }
            //this.SpeedVaribleList.Clear();
            //this.SpeedVaribleList.Clear();
            //this.ToolFrameInitializerList.Clear();
            //this.UserFrameInitializerList.Clear();
        }
    }
}
